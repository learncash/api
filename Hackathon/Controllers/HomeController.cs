﻿using Base.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hackathon.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginUser(string email, string pass)
        {
            var user = new Users().GetByCondition("WHERE email = @email AND pass = @pass AND isActive = 1", new { email, pass });
            if (user.Any())
                return Json(user.FirstOrDefault());
            else
                return Json("E-mail e/ou senha inválidos!");
        }

        [HttpGet]
        public ActionResult ListQuestionsByTypeId(int typeId)
        {
            return Json(new Question().GetByCondition("WHERE typeId = @typeId", new { typeId }), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ListTypes()
        {
            return Json(new TypeQuestion().All(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Answers(Answer obj)
        {
            try
            {
                obj.Insert();
                return Json("Resposta enviada!");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult CreateUser(Users obj)
        {
            try
            {
                obj.Insert();
                return Json("Usuário criado com sucesso!");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult CreateType(TypeQuestion obj)
        {
            try
            {
                obj.Insert();
                return Json("Tipo de questão criado com sucesso!");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult CreateQuestion(Question obj)
        {
            try
            {
                obj.Insert();
                return Json("Questão criada com sucesso!");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
    }
}