﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.DataBase
{
    [Table(Global.TablePrefix + "Answer")]
    public class Answer : DefaultDatabase<Answer>
    {
        public int id { get; set; }
        public int userId { get; set; }
        public int questionId { get; set; }
        public string answer { get; set; }
    }
}
