﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.DataBase
{
    [Table(Global.TablePrefix + "Client")]
    public class Client : DefaultDatabase<Client>
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
