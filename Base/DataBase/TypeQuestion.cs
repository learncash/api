﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.DataBase
{
    [Table(Global.TablePrefix + "TypeQuestion")]
    public class TypeQuestion : DefaultDatabase<TypeQuestion>
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
