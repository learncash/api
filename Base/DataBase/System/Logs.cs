﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.DataBase.System
{
    [Table(Global.TablePrefix + "Logs")]
    public class Logs : DefaultDatabase<Logs> 
    { 
        public DateTime Date { get; set;}
        public string Message { get; set;}
        public string Query { get; set; }
    }
}
