﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.DataBase
{
    [Table(Global.TablePrefix + "Question")]
    public class Question : DefaultDatabase<Question>
    {
        public int id { get; set; }
        public string description { get; set; }
        public int typeId { get; set; }
        public int clientId { get; set; }
    }
}
