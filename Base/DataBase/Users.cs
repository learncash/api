﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.DataBase
{
    [Table(Global.TablePrefix + "Users")]
    public class Users : DefaultDatabase<Users>
    {
        public int id { get; set; }
        public  string name { get; set; }
        public  string email { get; set; }
        public  string pass { get; set; }
        public  bool isActive { get; set; }
    }
}
