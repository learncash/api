﻿namespace Base
{
    public class DefaultDatabase<TEntity> : Connection<TEntity> where TEntity : class
    {
        public DefaultDatabase()
        {
            ConnectionString = System.Configuration.ConfigurationManager.
                     ConnectionStrings["defaultDatabase"].ConnectionString;
        }
    }
}